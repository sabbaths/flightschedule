<?php 
include_once('check_session.php');
?>

<!DOCTYPE html>
<html>
<title>SCHEDULING</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="javascript/jquery.min.js"></script>
<script src="javascript/script.js"></script>
<link rel="stylesheet" href="css/w3.css">
<link rel="stylesheet" href="css/user.css">
<body class="w3-white">

<?php

	include_once('nav.php');
	include_once('open_edit_modal.php');
	require('controller.php');
	require('database_model.php');

	$search_name = isset($_GET['search']) ? $_GET['search'] : "" ;

?>
    
<div class="w3-padding-large" id="main">
  <header class="w3-container w3-padding-32 w3-center w3-white" id="home">
  		<h2>SCHEDULES</h2>
	  		<?php
	  			echo "<div class='div_search'><p>

				<form action=''>
				  <input type='date' id='schedule_date' name='schedule_date'>
				  <input type='submit' value='Submit'>
				</form>
	  			";

	  			$user_date = !empty($_GET['schedule_date']) ? $_GET['schedule_date'] : "";
	  			$date = $user_date ? new DateTime(date($user_date)) : new DateTime(date("Y/m/d"));
	  			//$search_date = $user_date ? $user_date : $date->format('Y-m-d'); 
	  			$controller = new Controller();
	  			$controller->generateIndexScheduleTable($date->format('Y-m-d'));
		        for($i = 0; $i<7;$i++) {
		          $date->modify('+1 day');
		          $controller->generateIndexScheduleTable($date->format('Y-m-d'));  
		        }
	  		?>
  </header> 
</div>
<div id="loader" class="loader"></div> 

</body>
</html>
