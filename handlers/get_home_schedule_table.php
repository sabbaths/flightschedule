<?php

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

include_once('../database_model.php');
include_once('../controller.php');

$get_data_schedule = isset($_REQUEST['date']) ? $_REQUEST['date'] : date('Y-m-d');

$database = new Database();
$database->connectDB();

$schedules =  $database->getSchedules($get_data_schedule);
foreach($schedules as $key => $value) {
	$sched_id_2878 = $schedules[$key]["RP_C2878"];
	$sched_id_2589 = $schedules[$key]["RP_C2589"];
	$sched_id_3380 = $schedules[$key]["RP_C3380"];
	$sched_id_4442 = $schedules[$key]["RP_C4442"];

	if($sched_id_2878 != "") {
		$schedules[$key]["RP_C2878"] = $database->getScheduleDetails($sched_id_2878);
	} 
	if($sched_id_2589 != "") {
		$schedules[$key]["RP_C2589"] = $database->getScheduleDetails($sched_id_2589);
	}
	if($sched_id_3380 != "") {
		$schedules[$key]["RP_C3380"] = $database->getScheduleDetails($sched_id_3380);
	}
	if($sched_id_4442 != "") {
		$schedules[$key]["RP_C4442"] = $database->getScheduleDetails($sched_id_4442);
	}
}

/*
route to controller
*/

$response_data = $schedules;
echo json_encode($schedules);

?>