<?php

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

include_once('../database_model.php');

$schedule_id = isset($_REQUEST['schedule_id']) ? $_REQUEST['schedule_id'] : 0;

$database = new Database();
$database->connectDB();

$schedules =  $database->getScheduleDetails($schedule_id);

$response_data = $schedules;
echo json_encode($response_data);

?>