<?php

class DatabaseConnection 
{

private static $environment = 1; //1 for dev, 2 godaddy 000webhostapp
private $connection = null;
private static $instance = null;
private static $servername = "";
private static $username = "";
private static $password = "";
private static $database = "";

    private function __construct() 
    {
        // Create connection
        $conn = new mysqli(self::$servername, self::$username, self::$password);
        $conn->select_db(self::$database);
        $this->connection = $conn;

        // Check connection
        if ($conn->connect_error) 
        {
            $status_code = 901;
            die("Connection failed: " . $conn->connect_error);
        }
    }

    public static function getInstance($servername, $username, $password, $database) 
    {
        self::$servername = $servername;
        self::$username = $username;
        self::$password = $password;
        self::$database = $database;

        if (self::$instance == null)
        {
          self::$instance = new DatabaseConnection();
        }

        return self::$instance;
    }

    public function getConnection() 
    {
        return $this->connection;
    }

}

?>