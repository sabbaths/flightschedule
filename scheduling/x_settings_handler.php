<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$post_data = file_get_contents("php://input");
$json_data = json_decode($post_data);

include_once('database_model.php');

$announcement = isset($_POST['password']) ? $_POST['password'] : null; 
$announcement = $json_data->announcement;


$database = new Database();
$database->connectDB();

$status_code = $database->announcement($announcement);

$response_data = [ 'status_code' => $status_code[0]];
echo json_encode($status_code[0]); 

?>